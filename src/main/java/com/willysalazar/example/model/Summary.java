package com.willysalazar.example.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Summary {
    private String description;
    private String available;
    private String unitSpecialPrice;
    private String unitPriceDiscount;
    private String cartTotal;
    private String qty;
    private String totalPrice;
}
