package com.willysalazar.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static com.willysalazar.example.seleniumAbstract.WaitElement.waitForPresenceOfElementLocated;
import static com.willysalazar.example.seleniumAbstract.WaitElement.waitVisibilityOf;

public class CategoryPage {
    private WebDriver driver;
    private Actions action;


    public CategoryPage(WebDriver webDriver){
        this.driver = webDriver;
        action = new Actions(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li//span[contains(text(), \"Add to cart\")]")
    private WebElement btnAddToCart;

    @FindBy(xpath = "//span[contains(text(), \"Proceed to checkout\")]")
    private WebElement btnProceedToCheckout;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]")
    private WebElement btnDresses;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]//a[contains(text(), \"Summer Dresses\")]")
    private WebElement btnSummerDresses;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li")
    private List<WebElement> lisOfItemsProduct;

    @FindBy(xpath = "//b[contains(text(), \"Cart\")]/..")
    private WebElement tabCart;


    public CategoryPage moveToButtonDresses(){
        action.moveToElement(btnDresses).perform();
        return this;
    }
//
    public CategoryPage clickButtonSummerDresses(){
        waitVisibilityOf(driver,btnSummerDresses).click();
        return this;
    }

    public CategoryPage moveToButtonSummerDresses(){
        action.moveToElement(btnSummerDresses).perform();
        return this;
    }

    private WebElement getItemSummerDressProduct(int idDress) {
        return waitForPresenceOfElementLocated(driver, By.xpath("//*[@id=\"center_column\"]/ul/li[" + idDress + "]"));
    }

    public CategoryPage moveToSummerDressesProduct(){
        action.moveToElement(getItemSummerDressProduct(1)).perform();
        return this;
    }

    public CategoryPage moveToButtonAddToCart(){
        action.moveToElement(btnAddToCart).perform();
        return this;
    }

    public CategoryPage clickAndBuildButtonAddToCart(){
        action.click(btnAddToCart).build().perform();
        return this;
    }

    public CategoryPage clickAndBuildButtonProceedToCheckout(){
        action.click(waitVisibilityOf(driver, btnProceedToCheckout)).build().perform();
        return this;
    }



}
