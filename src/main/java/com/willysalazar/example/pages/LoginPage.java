package com.willysalazar.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.willysalazar.example.seleniumAbstract.WaitElement.waitVisibilityOf;

public class LoginPage {
    private WebDriver driver;

    public LoginPage(WebDriver webDriver){
        this.driver = webDriver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "email")
    private WebElement fieldEmail;

    @FindBy(id = "passwd")
    private WebElement fieldPassword;

    @FindBy(id = "SubmitLogin")
    private WebElement btnSignIn;

    public LoginPage fillEmail(String email){
        waitVisibilityOf(driver, fieldEmail).sendKeys(email);
        return this;
    }

    public LoginPage fillPassword(String password){
        waitVisibilityOf(driver, fieldPassword).sendKeys(password);
        return this;
    }

    public LoginPage signIn(){
        waitVisibilityOf(driver,btnSignIn).click();
        return this;
    }

}
