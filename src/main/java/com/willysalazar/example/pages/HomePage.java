package com.willysalazar.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private WebDriver driver;

    public HomePage(WebDriver webDriver) {
        this.driver = webDriver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[contains(text(),'Sign in')]")
    private WebElement btnSignIn;

    @FindBy(xpath = "//a[contains(text(),'Sign out')]")
    private WebElement btnSignOut;

    @FindBy(xpath = "//header/div[3]/div[1]/div[1]/div[6]/ul[1]/li[1]/a[1]")
    private WebElement menuWomen;

    public HomePage signIn(){
        btnSignIn.click();
        return this;
    }

    public HomePage signOut(){
        btnSignIn.click();
        return this;
    }


}
