package com.willysalazar.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.willysalazar.example.seleniumAbstract.SeleniumUtils.clickUsingJs;
import static com.willysalazar.example.seleniumAbstract.WaitElement.waitForPresenceOfElementLocated;
import static com.willysalazar.example.seleniumAbstract.WaitElement.waitVisibilityOf;

public class ShoppingCartPage {

    private WebDriver driver;
    private Actions action;

    public ShoppingCartPage(WebDriver webDriver){
        this.driver = webDriver;
        action = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//p/a/span[contains(text(), \"Proceed to checkout\")]")
    private WebElement btnProceedToCheckoutInSummary;

    @FindBy(xpath = "//*[@id=\"center_column\"]/form/p/button")
    private WebElement btnProceedToCheckoutInAddresses;

    @FindBy(xpath = "//*[@id=\"form\"]/p/button")
    private WebElement btnProceedToCheckoutInShipping;

    @FindBy(xpath = "//td/p/a[contains(text(), \"Printed Summer Dress\")]")
    private WebElement textDescription;

    @FindBy(xpath = "//span[contains(text(),'In stock')]")
    private WebElement textAvailable;

    @FindBy(xpath = "//*[@class=\"cart_unit\"]/span/span[@class='price special-price']")
    private WebElement textUnitSpecialPrice;

    @FindBy(xpath = "//span[contains(text(),'-5%')]")
    private WebElement textUnitDiscount;

    @FindBy(xpath = "//*[@class=\"cart_total\"]/span")
    private WebElement textCartTotal;

    @FindBy(css = "#summary_products_quantity")
    private WebElement textQty;

    @FindBy(xpath = "//span[@id='total_price']")
    private WebElement textTotalPrice;

    @FindBy(id = "cgv")
    private WebElement checkboxTerms;

    @FindBy(xpath = "//a[@title='Pay by bank wire']")
    private WebElement btnPaymentWire;

    @FindBy(xpath = "//*[@id='cart_navigation']/button")
    private WebElement btnConfirmOrder;

    @FindBy(xpath = "//*[@id=\"center_column\"]/div/p/strong")
    private WebElement textMytStoreComplete;

    public ShoppingCartPage clickButtonProceedToCheckoutInSummary(){
        action.click(waitVisibilityOf(driver, btnProceedToCheckoutInSummary)).build().perform();
        return this;
    }
    public ShoppingCartPage clickButtonProceedToCheckoutInAddresses(){
        action.click(waitVisibilityOf(driver, btnProceedToCheckoutInAddresses)).build().perform();
        return this;
    }

    public ShoppingCartPage clickButtonProceedToCheckoutInShipping(){
        action.click(waitVisibilityOf(driver, btnProceedToCheckoutInShipping)).build().perform();
        return this;
    }

    public String getTextDescription(){
        return waitVisibilityOf(driver,textDescription).getText();
    }

    public String getTextAvailable(){
        return waitVisibilityOf(driver,textAvailable).getText();
    }

    public String getTextUnitSpecialPrice(){
        return waitVisibilityOf(driver,textUnitSpecialPrice).getText();
    }

    public String getTextUnitDiscount(){
        return waitVisibilityOf(driver,textUnitDiscount).getText();
    }

    public String getTextCartTotal(){
        return waitVisibilityOf(driver, textCartTotal).getText();
    }

    public String getTextQty(){
        return waitVisibilityOf(driver, textQty).getText();
    }

    public String getTextTotalPrice(){
        return waitVisibilityOf(driver,textTotalPrice).getText();
    }

    public ShoppingCartPage stepByNameIsDisplayed(String stepName) {
        waitForPresenceOfElementLocated(driver, By.xpath("//div[@id='center_column']/h1[contains(text(),'"+ stepName+"')]")).isDisplayed();
        return this;
    }

    public ShoppingCartPage stepShippingIsDisplayed() {
        waitForPresenceOfElementLocated(driver, By.xpath("//div[@id='center_column']/div/h1[contains(text(),'Shipping')]")).isDisplayed();
        return this;
    }

    public ShoppingCartPage clickOfAcceptTerms(){
        checkboxTerms.click();
        return this;
    }

    public ShoppingCartPage clickButtonPayment(){
        btnPaymentWire.click();
        return this;
    }

    public ShoppingCartPage confirmOrder(){
        btnConfirmOrder.click();
        return this;
    }

    public String getTextMyStoreComplete(){
        return waitVisibilityOf(driver,textMytStoreComplete).getText();
    }



}
