package com.willysalazar.example.provider;

import com.willysalazar.example.model.Summary;

public class SummaryProvider {

    public static Summary summaryExpected(){
        return Summary
                .builder()
                .description("Printed Summer Dress")
                .available("In stock")
                .unitSpecialPrice("$28.98")
                .unitPriceDiscount(" -5% ")
                .cartTotal("$28.98")
                .qty("1 Product")
                .totalPrice("$30.98")
                .build();
    }
}
