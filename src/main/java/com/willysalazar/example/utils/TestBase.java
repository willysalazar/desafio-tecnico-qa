package com.willysalazar.example.utils;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import com.willysalazar.example.driver.DriverFactory;
import com.willysalazar.example.steps.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import com.willysalazar.example.report.ReportListener;

import static com.willysalazar.example.utils.PropertyLoader.returnConfigValue;

@Listeners({ExtentITestListenerClassAdapter.class, ReportListener.class})
public abstract class  TestBase {

    protected static WebDriver driver;
    protected Actions action;

    protected HomeSteps homeSteps;
    protected LoginSteps loginSteps;
    protected MyAccountSteps accountSteps;
    protected CategorySteps categorySteps;
    protected ShoppingCartSteps shoppingCartSteps;

    public static WebDriver getDriver() {
        return driver;
    }

    @BeforeMethod
    public void preCondition() {
        driver = new DriverFactory().createInstance();
        driver.manage().window().maximize();
        driver.get(returnConfigValue("url.base"));
        homeSteps = new HomeSteps(driver);
        loginSteps = new LoginSteps(driver);
        accountSteps = new MyAccountSteps(driver);
        categorySteps = new CategorySteps(driver);
        shoppingCartSteps = new ShoppingCartSteps(driver);
    }

    @AfterMethod
    public void postCondition(){
        driver.quit();
    }
}