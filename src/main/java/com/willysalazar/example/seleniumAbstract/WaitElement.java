package com.willysalazar.example.seleniumAbstract;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitElement {

    public static WebElement waitVisibilityOf(WebDriver driver, WebElement webElement) {
        int timeWait = 10;
        WebDriverWait explicitWaitByElement = new WebDriverWait(driver, timeWait);
        explicitWaitByElement.until(ExpectedConditions.visibilityOf(webElement));
        return webElement;

    }

    public static WebElement waitForPresenceOfElementLocated(WebDriver driver, By selector) {
        int timeWait = 10;
        WebElement element = (new WebDriverWait(driver, timeWait)).until(ExpectedConditions.presenceOfElementLocated(selector));
        return element;
    }
}
