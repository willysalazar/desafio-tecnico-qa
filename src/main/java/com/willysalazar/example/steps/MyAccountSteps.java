package com.willysalazar.example.steps;

import com.willysalazar.example.pages.MyAccountPage;
import org.openqa.selenium.WebDriver;

public class MyAccountSteps extends MyAccountPage {

    public MyAccountSteps(WebDriver webDriver) {
        super(webDriver);
    }

    public String returnTitleMyAccount(){
       return getTitle();
    }

}
