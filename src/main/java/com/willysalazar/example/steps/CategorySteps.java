package com.willysalazar.example.steps;

import com.willysalazar.example.pages.CategoryPage;
import org.openqa.selenium.WebDriver;

public class CategorySteps extends CategoryPage {

    public CategorySteps(WebDriver webDriver) {
        super(webDriver);
    }

    public void selectSummerDressesProductsToCart(){
        moveToButtonDresses()
        .moveToButtonSummerDresses()
        .clickButtonSummerDresses()
        .moveToSummerDressesProduct()
        .moveToButtonAddToCart()
        .clickAndBuildButtonAddToCart()
        .clickAndBuildButtonProceedToCheckout();
    }

}
