package com.willysalazar.example.steps;

import com.willysalazar.example.pages.HomePage;
import org.openqa.selenium.WebDriver;

public class HomeSteps extends HomePage {

    public HomeSteps(WebDriver webDriver) {
        super(webDriver);
    }

    public void goSignIn(){
        signIn();
    }

}
