package com.willysalazar.example.steps;

import com.willysalazar.example.model.Summary;
import com.willysalazar.example.pages.ShoppingCartPage;
import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ShoppingCartSteps extends ShoppingCartPage {
    public ShoppingCartSteps(WebDriver webDriver) {
        super(webDriver);
    }

    public Summary returnSummary(){
        return Summary
                .builder()
                .description(getTextDescription())
                .available(getTextAvailable())
                .unitSpecialPrice(getTextUnitSpecialPrice())
                .unitPriceDiscount(getTextUnitDiscount())
                .cartTotal(getTextCartTotal())
                .qty(getTextQty())
                .totalPrice(getTextTotalPrice())
                .build();
    }
    public void validateSummary(Summary summaryExpected){
        stepByNameIsDisplayed("Shopping-cart summary");
        assertEquals(returnSummary(), summaryExpected);
    }

    public void proceedToCheckout(){
        clickButtonProceedToCheckoutInSummary();
             stepByNameIsDisplayed("Addresses")
        .clickButtonProceedToCheckoutInAddresses()
                .stepShippingIsDisplayed()
        .clickOfAcceptTerms()
        .clickButtonProceedToCheckoutInShipping()
        .stepByNameIsDisplayed("Please choose your payment method")
        .clickButtonPayment()
        .stepByNameIsDisplayed("Order summary")
        .confirmOrder()
        .stepByNameIsDisplayed("Order confirmation");
    }

    public String messageOrderIsCompleted(){
        return getTextMyStoreComplete();
    }

}
