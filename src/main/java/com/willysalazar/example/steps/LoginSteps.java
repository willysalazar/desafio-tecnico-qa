package com.willysalazar.example.steps;

import com.willysalazar.example.model.User;
import com.willysalazar.example.pages.LoginPage;
import org.openqa.selenium.WebDriver;

public class LoginSteps extends LoginPage {

    public LoginSteps(WebDriver webDriver) {
        super(webDriver);
    }

    public void doLogin(User user){
    fillEmail(user.getEmail())
        .fillPassword(user.getPassword())
            .signIn();
    }

}
