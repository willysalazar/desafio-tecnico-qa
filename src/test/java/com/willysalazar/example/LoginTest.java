package com.willysalazar.example;

import com.willysalazar.example.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;
import com.willysalazar.example.utils.TestBase;

import static com.willysalazar.example.provider.UserProvider.buildUserSuccess;
import static org.testng.Assert.assertEquals;

public class LoginTest extends TestBase {
    private static String TITLE_MY_ACCOUNT = "My account - My Store";
    private static final Logger LOGGER = LogManager.getLogger();


    @Test
    public void shouldBeLoginWithSuccess() {
        User user = buildUserSuccess();

        homeSteps.goSignIn();
        loginSteps.doLogin(user);

        assertEquals(accountSteps.returnTitleMyAccount(), TITLE_MY_ACCOUNT);
        LOGGER.info("Login realizado com sucesso utilizando o email: " +user.getEmail());
    }
}
