package com.willysalazar.example;

import com.willysalazar.example.model.Summary;
import com.willysalazar.example.model.User;
import com.willysalazar.example.utils.TestBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import static com.willysalazar.example.provider.SummaryProvider.summaryExpected;
import static com.willysalazar.example.provider.UserProvider.buildUserSuccess;
import static org.testng.Assert.assertEquals;

public class ShoppingTest extends TestBase {
    private static final Logger LOGGER = LogManager.getLogger();

    @Test
    public void shouldBeShoppingClothesWithSuccess() {
        User user = buildUserSuccess();
        Summary summaryExpected = summaryExpected();
        homeSteps.goSignIn();
        loginSteps.doLogin(user);
        categorySteps.selectSummerDressesProductsToCart();
        shoppingCartSteps.validateSummary(summaryExpected);
        shoppingCartSteps.proceedToCheckout();
        assertEquals(shoppingCartSteps.messageOrderIsCompleted(),"Your order on My Store is complete." );
        LOGGER.info("Compra do produto"
                + summaryExpected.getDescription() +
                " foi realizada com sucesso no valor total: " + summaryExpected.getTotalPrice());
    }
}
