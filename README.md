# Projeto de Automação :: desafio-tecnico-qa

- Projeto desenvolvido para realizar os testes automatizados do site automationpractice.com
- Os testes estão confiugurados no modo: headless (sem abrir o browser, para rodar na pipeline)

## Team
* willysalazar

## Repositórios
* *github* - [GitHub](https://github.com/willysalazar/desafio-tecnico-qa) sem pipeline integrada.
* *gitlab* - [GitLab](https://gitlab.com/willysalazar/desafio-tecnico-qa) com pipeline integrada.

### Lista de Funcionalidades testadas
Este projeto tem como objetivo garantir o fluxo de teste funcional integrado (2e2)
* Login
* Shopping

## Estrutura

### Dependencias
Possui algumas dependencias: 
* *selenium* - [Selenium](https://www.selenium.dev/) framework para criar testes automatizados de sistemas Web. 
* *testng* - O framework [TestNG](https://testng.org/) engine de teste, que possui funcionalidades semelhantes ao JUnit e NUnit. 
* *extentreports* - A biblioteca [extentreports](http://www.extentreports.com/) gera os relatórios de teste em tempo real para que você possa analisar seus testes de uma maneira totalmente diferente. 
* *webdrivermanager* - A biblioteca [webdrivermanager](https://github.com/bonigarcia/webdrivermanager) permite automatizar o gerenciamento dos drivers (por exemplo, chromedriver, geckodriver, etc.) exigidos pelo Selenium WebDriver. 
* *lombok* - A biblioteca [Lombok](https://projectlombok.org/) permite automatizar a geração de getters, setters, builders, construtores de forma fácil. 

### Compiladores
Possui duas formas de compilar este projeto:
* *apache-maven* -O sistema de compilacao [Apache Maven](https://maven.apache.org/) esta sendo utilizado para realizar a compilacao e build/test do projeto.

## Execucao Automatizada (GitLab-CI)

### Passo-a-passo execucao automatizada
* Acessar omenu CI/CD > Pipelines
* Clicar no botão 'Run Pipeline'
* Preencher os dados da branch em 'Run for: master'
* Clicar no botão 'Run Pipeline'
* Apos execução, acessar 'Job artifacts' para validar os artefatos

## Execucao Manual

### Passo-a-passo execucao manual MAVEN
#### *PRE-REQUISITO:* 
Deve-se ter instalado em sua maquina local:
- GIT
- MAVEN
- Java8 

IMPORTANTE: Deve-se ter um repositório maven devidamente configurado. [Para saber mais acesse aqui](https://maven.apache.org/guides/mini/guide-configuring-maven.html). 

* Abrir prompt(shell) do sistema

```
	$ git clone https://github.com/willysalazar/desafio-tecnico-qa.git
```
* Acessar raiz do projeto

```
	$ cd /diretorio//desafio-tecnico-qa
```
* Realizar comando para executar todos os testes do projeto

```
	$ mvn clean test
```
* Realizar comando para executar apenas o teste de login

```
	$ mvn clean test -Dtest=LoginTest
```
* Realizar comando para executar apenas o teste de compras

```
	$ mvn clean test -Dtest=ShoppingTest
```

## Relatórios dos Testes
* Para visualizar o relatorio dos testes, deve-se acessar o arquivo: */target/report/test_execution.html*

### Passo-a-passo verificacao dos relatorios dos testes
* Executar os testes
* Acessar o arquivo *test_execution.html* dentro do diretorio target do projeto

```
	$ cd /diretorio/desafio-tecnico-qa/target/report/test_execution.html
```

